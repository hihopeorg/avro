## v1.0.1

- 适配DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）

## v1.0.0


- Avro支持的数据类型
```
      Type
      原始类型：BooleanType, DoubleType, FloatType, IntType, LongType, NullType, StringType
      复杂类型：ArrayType, EnumType,  MapType 
```
- Avro暂不支持的类型
```
      Type
      原始类型：BytesType
      复杂类型： FixedType,  RecordType, UnionType, LogicalType
```