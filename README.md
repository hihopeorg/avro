# avro

## 简介

> 数据序列化系统，支持丰富的数据结构，将其转化成便于存储或传输的二进制数据格式。它同时也是一个容器文件，用于存储持久数据。

## 效果展示：

![动画](./avro.gif)

## 下载安装

```shell
npm i avro-js
npm i buffer
npm i util

```

OpenHarmony
npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。

## 使用说明

### 示例代码

1. 在page页面中引入avro

```
import avro from 'avro-js-ohos/etc/browser/avro'
```

2. 使用

```
    var mapType = avro.parse({ 'type': 'map', values: 'long' })
    var petMap = { 'key1': 12343, 'key2': 3445 };
    var bufMap = mapType.toBuffer(petMap); // Buffer containing 'Hi''s Avro encoding.
    var mapSrc = mapType.fromBuffer(bufMap); // === 'Hi'
    var bufferStr = "";
    for (var i = 0; i < bufMap.length; i++) {
      bufferStr = bufferStr + bufMap[i];
    }
    this.mapTestResult="MapType:{原始数据:" + JSON.stringify(petMap)  + ",序列化后:" + bufferStr + ",还原数据:" + JSON.stringify(mapSrc)+ "};";
    console.info("----MapType--{original_data:" + JSON.stringify(petMap) + ",serialization：" + bufferStr + ",restore_data：" + JSON.stringify(mapSrc) + "}");
```

## 接口说明

1. 利用parse(schema, [opts]) API，解析schema并返回对应的实例 Type。
   ` avro.parseparse(schema, opts)`
2. 利用对应的实例Type .toBuffer(val) API，进行编码。
   `.toBuffer(val)`
3. 利用对应的实例Type .fromBuffer(buf, [resolver,] [noCheck]) API，进行解码。
   `.fromBuffer(buf, [resolver,] [noCheck])`

## 约束与限制

在下述版本验证通过：

- DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）

## 目录结构

````
|---- avro  
|     |---- entry  # 示例代码文件夹
|     |---- README.md  # 安装使用方法                    
````

## 三方库适配说明
以下所使用的版本依赖与源库版本号保持一致：

| 原始库        |  版本号  |
| --------   |  :----:  |
| [util](https://github.com/browserify/node-util)       |  1.0.0     |
| [avro-js](https://github.com/apache/avro/tree/master/lang/js)    |    ^1.11.1   |


1.util 做了适配,
修改处为原库路径node_modules/util/util.js 第 109 行 
修改前
````
if (process.env.NODE_DEBUG) {
  var debugEnv = process.env.NODE_DEBUG;
  debugEnv = debugEnv.replace(/[|\\{}()[\]^$+?.]/g, '\\$&')
    .replace(/\*/g, '.*')
    .replace(/,/g, '$|^')
    .toUpperCase();
  debugEnvRegex = new RegExp('^' + debugEnv + '$', 'i');
}

````
修改后为

````
if (typeof process !== 'undefined' && process.env.NODE_DEBUG) {
  var debugEnv = process.env.NODE_DEBUG;
  debugEnv = debugEnv.replace(/[|\\{}()[\]^$+?.]/g, '\\$&')
    .replace(/\*/g, '.*')
    .replace(/,/g, '$|^')
    .toUpperCase();
  debugEnvRegex = new RegExp('^' + debugEnv + '$', 'i');
}
````
2.avro-js 做了适配,
项目中需要使用使用util依赖
(1)修改处：
在 avro-js/package.json ,添加 "util": "file:../util" 依赖
````
  "dependencies": {
    "underscore": "^1.13.2",
    "util": "file:../util"
  },
````


## 贡献代码

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/avro/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/avro/pulls)
。

## 开源协议

本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/avro/blob/master/LICENSE) ，请自由地享受和参与开源。





  
